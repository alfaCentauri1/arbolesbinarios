package arboles.binarios;

public class ArbolBinario {
    private Nodo raiz;

    public ArbolBinario() {
        raiz = null;
    }

    public void insertar(int dato){
        Nodo nuevo = new Nodo(dato);
        if (raiz == null){
            raiz = nuevo;
        }
        else{
            Nodo actual = raiz;
            Nodo padre = raiz;
            while (true){
                padre = actual;
                if(dato < actual.getDato()){
                    actual = actual.getHijoIzquierdo();
                    if (actual == null){
                        padre.setHijoIzquierdo(nuevo);
                        return;
                    }
                }
                else {
                    actual = actual.getHijoDerecho();
                    if (actual == null){
                        padre.setHijoDerecho(nuevo);
                        return;
                    }
                }
            }
        }
    }

    public boolean vacio(){
        return (raiz==null);
    }

    public void ver(Nodo nodo){
        if (nodo != null){
            ver(nodo.getHijoIzquierdo());
            System.out.println(nodo.getDato());
            ver(nodo.getHijoDerecho());
        }
    }

    public Nodo getRaiz() {
        return raiz;
    }

    public void setRaiz(Nodo raiz) {
        this.raiz = raiz;
    }
}
