package arboles.binarios;

import java.util.Iterator;
import java.util.Scanner;
import java.util.TreeSet;

public class Ejecutable {
    public static void main(String args[]){
        int entrada = 0;
        System.out.println("Ejemplo de arboles en Java 18.");
        Scanner console = new Scanner(System.in);
        ArbolBinario arbolBinario = new ArbolBinario();
        do{
            System.out.println("Escriba un número(-1 para salir): ");
            entrada = console.nextInt();
            if (entrada > 0)
                arbolBinario.insertar(entrada);
        }while(entrada > 0);
        arbolBinario.ver(arbolBinario.getRaiz());
        //TreeSet
        System.out.println("Utilizando la clase TreeSet.");
        TreeSet<Integer> treeSet = new TreeSet<>();
        treeSet.add(1);
        treeSet.add(10);
        treeSet.add(7);
        treeSet.add(5);
        treeSet.add(8);
        for (Iterator<Integer> i = treeSet.iterator(); i.hasNext(); ){
            System.out.println(i.next());
        }
        System.out.println("Fin del programa.");
    }
}
